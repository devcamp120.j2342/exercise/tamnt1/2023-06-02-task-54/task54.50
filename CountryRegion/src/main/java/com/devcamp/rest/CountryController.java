package com.devcamp.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class CountryController {
    @GetMapping("/countries")
    public void getCountries() {
        List<Country> countries = initializeCountries();

        for (Country country : countries) {
            System.out.println("Country Code: " + country.getCountryCode());
            System.out.println("Country Name: " + country.getCountryName());

            if (country.getCountryCode().equals("VN")) {
                System.out.println("Regions (Provinces/Cities):");
                for (Region region : country.getRegions()) {
                    System.out.println("Region Code: " + region.getRegionCode());
                    System.out.println("Region Name: " + region.getRegionName());
                }
            }

            System.out.println("------------------------------");
        }
    }

    private List<Country> initializeCountries() {
        ArrayList<Country> countries = new ArrayList<>();

        ArrayList<Region> vietnamRegions = new ArrayList<>();
        vietnamRegions.add(new Region("HN", "Hanoi"));
        vietnamRegions.add(new Region("HCM", "Ho Chi Minh City"));

        ArrayList<Region> emptyRegions = new ArrayList<>();

        countries.add(new Country("VN", "Vietnam", vietnamRegions));
        countries.add(new Country("US", "United States", emptyRegions));
        countries.add(new Country("JP", "Japan", emptyRegions));

        return countries;
    }

}
